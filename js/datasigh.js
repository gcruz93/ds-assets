const _ds = {
  onload() {
    this.loader.remove(document.body)

    if (document.body.classList.contains('with-header-onscroll')) {
      window.addEventListener('scroll', e => {
        this.toggle.showHeaderOnScroll()
      })
    }

    document.addEventListener('click', e => {
      const target = e.target

      if (target.classList.contains('toggleFullScreen')) {
        this.toggle.fullScreen()
      }
    }, false)
  },

  loader: {
    add(target) {
      target.classList.add('with-loader')
    },
    addDisabled(target) {
      target.classList.add('with-loader-disabled')
    },
    remove(target) {
      target.classList.remove('with-loader')
      target.classList.remove('with-loader-disabled')
    },
  },
  helpers: {
    debounce(func, wait, immediate) {
      var timeout;
      return function () {
        var context = this, args = arguments;
        var later = function () {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    },
    getOffset(target) {
      const rect = target.getBoundingClientRect()
      return {
        top: rect.top - document.body.scrollTop,
        left: rect.left - document.body.scrollTop
      }
    },
    date: {
      monthName(value) {
        const month = value instanceof Date ? value.getMonth() + 1 : value
        switch (month) {
          case 1: return 'Janeiro'
          case 2: return 'Fevereiro'
          case 3: return 'Março'
          case 4: return 'Abril'
          case 5: return 'Maio'
          case 6: return 'Junho'
          case 7: return 'Julho'
          case 8: return 'Agosto'
          case 9: return 'Setembro'
          case 10: return 'Outubro'
          case 11: return 'Novembro'
          case 12: return 'Dezembro'
          default: return ''
        }
      },
      dayName(value) {
        const day = value instanceof Date ? value.getDay() : value
        switch (day) {
          case 0: return 'Segunda-feira'
          case 1: return 'Terça-feira'
          case 2: return 'Quarta-feira'
          case 3: return 'Quinta-feira'
          case 4: return 'Sexta-feira'
          case 5: return 'Sábado'
          case 6: return 'Domingo'
          default: return ''
        }
      }
    },
  },
  overlay: {
    show() {
      document.querySelector('.ds-overlay').classList.add('active')
    },
    hide() {
      document.querySelector('.ds-overlay').classList.remove('active')
    },
  },
  toggle: {
    fullScreen() {
      var doc = window.document
      var docEl = doc.documentElement
      var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen
      var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen
      if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) requestFullScreen.call(docEl)
      else cancelFullScreen.call(doc)
    },
    showHeaderOnScroll() {
      const $header = document.querySelector('.ds-header')
      const distTop = document.documentElement.scrollTop

      if (distTop >= 54) $header.classList.add('with-scroll')
      else {
        if ( window.innerWidth > 992 ) {
          if (distTop <= 1) $header.classList.remove('with-scroll')
        } else {
          if (distTop <= 54) {
            if ($header.classList.contains('with-middle')) {
              $header.classList.remove('with-scroll')
            } else if (distTop <= 1) $header.classList.remove('with-scroll')
          }
        }
      }
    },
  },
  form: {
    serialize(form) {
      /*!
      * Serialize all form data into a query string
      * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
      * @param  {Node}   form The form to serialize
      * @return {String}      The serialized form data
      */

        // Setup our serialized data
        var serialized = [];

        // Loop through each field in the form
        for (var i = 0; i < form.elements.length; i++) {

          var field = form.elements[i];

          // Don't serialize fields without a name, submits, buttons, file and reset inputs, and disabled fields
          if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;

          // If a multi-select, get all selections
          if (field.type === 'select-multiple') {
            for (var n = 0; n < field.options.length; n++) {
              if (!field.options[n].selected) continue;
              serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[n].value));
            }
          }

          // Convert field data to a query string
          else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
            serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value));
          }
        }
        return serialized.join('&');
    },
    getData(form) {
      var formData = {}

      if (typeof form === 'string' || form instanceof String) form = document.forms[form]

      for (var i = 0; i < form.elements.length; i++) {
        var field = form.elements[i]

        if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue

        if (field.type === 'select-multiple') {
          for (var n = 0; n < field.options.length; n++) {
            if (!field.options[n].selected) continue
            formData[encodeURIComponent(field.name)] = encodeURIComponent(field.options[n].value)
          }
        }

        else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
          formData[encodeURIComponent(field.name)] = encodeURIComponent(field.value)
        }
      }

      return formData
    },
    setData(form, data) {
      if (typeof form === 'string' || form instanceof String){
        form = document.forms[form]
      }

      for (var i = 0; i < form.elements.length; i++) {
        var field = form.elements[i]

        if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue

        for (key in data) {
          const value = data[key]
          if(field.name == key) {
            form.elements[key].value = value
          }
        }
      }
    },
  },
  sort: {
    int(property) {
      var sortOrder = 1

      if (property[0] === "-") {
        sortOrder = -1
        property = property.substr(1)
      }

      return function(a, b) {
        if (sortOrder == -1) {
          return b[property] - a[property]
        } else {
          return a[property] - b[property]
        }
      }
    },
    str(property) {
      var sortOrder = 1;

      if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
      }

      return function (a, b) {
        if (sortOrder == -1) {
          return b[property].localeCompare(a[property]);
        } else {
          return a[property].localeCompare(b[property]);
        }
      }
    }
  },
}
window.addEventListener('load', _ds.onload())

// $(window).on("scroll", function () {
//   let scrollTop = $(window).scrollTop(),
//     elementOffset = $('#content').offset().top,
//     distance = (elementOffset - scrollTop) + 10;

//   if (distance <= 0) $('#card-info').addClass('flutuando');
//   else $('#card-info').removeClass('flutuando');
// })
